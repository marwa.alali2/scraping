import requests
#from PIL import Image
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
from datetime import datetime


prefix_page = "https://www.okaidi.fr/" 
dic_produit = {}
data_page = ["pyjama-2-pieces-en-velours-bleu-navy-124329","gilet-brassiere-maille-reversible-rose-valeriane-126208",
            "bonnet-tricot-cotele-raye-gris-chine-clair-125668","robe-maille-a-pois-velours-rouge-cerise-125487",
             "robe-de-fete-en-velours-imprime-dore-bleu-navy-125511","jupe-courte-tutu-a-paillettes-bleu-navy-125518",
            "baskets-bimatiere-a-scratchs-bleu-navy-125679","sweat-a-message-hiver-bleu-marine-125672"]
       


for it in data_page:
    
    page = requests.get(prefix_page+it)
    soup = BeautifulSoup(page.content, 'html.parser')
    name_product= soup.find(class_="item-title")

    for txt in name_product.strings:
        txt = re.sub('  ', '', txt)
        print(txt)
    pro_id = it.split('-')[-1]
    prix0 = soup.find(class_='product-price-item price-value-'+pro_id)
    for pri in prix0.strings:
        pri = re.sub('  ', '', pri)
        pri = pri.replace(",", ".")
        print(pri)
    #print(soup.find("description-wrapper accordionItem--content"))

    ref_produit2=soup.find('span', attrs={'id': 'sku-'+pro_id})
    if ref_produit2:
        
        for ref in ref_produit2.strings:
            ref = re.sub('  ', '', ref)
            ref = ref.replace(":", "")
            print(ref)
    else:
        ref=""
    txt = txt.replace("\n", "")
    txt = txt.replace("\r", "")
    pri = pri.replace("\n", "")
    pri = pri.replace("\r", "")
    dic_produit.update({txt:(pri.strip("€")+","+ref+","+str(datetime.now())+","+"id_ok")})

#print(dic_produit)
line=""

with open("Product_okaidi.csv", 'w',encoding="utf-8") as f:
    for key,value in dic_produit.items():
      
        print(key+','+value)
        line +=  key+','+ value +'\n'
    f.write(line)